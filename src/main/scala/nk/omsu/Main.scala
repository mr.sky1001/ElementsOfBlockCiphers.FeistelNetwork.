package nk.omsu
import scala.util.Random
import java.io._
import java.nio.ByteBuffer
import java.util

/**
  * Created by nk16 on 06.09.16.
  */

object Main {
  var s  = 4
  var  a: Array[Byte] = null
  var f:Boolean = true
   var f2:Boolean = true
  /*def generateKeys(): util.BitSet = {
    val key2 = new util.BitSet()
    key2.set(0);key2.set(3)
    key2
  }*/
    def readBytes(file:File):Array[Byte] = {
    val is = new FileInputStream(file)
    val bytesInp = new Array[Byte](file.length.toInt)
    is.read(bytesInp)
    is.close()
    bytesInp
  }
  def printStr(fileName: String, array: Array[Byte]): Unit ={
    val out = new FileOutputStream(new File(fileName))
    out.write(a)
    out.close()
  }
  def generateKey(): util.BitSet = {
    val key = new util.BitSet()
    key.set(0)
    key.set(0)
    key.set(1)
    key.set(0)
    key.set(1)
    key.set(0)
    key.set(1)
    key.set(0)
    key
  }
  def byteArrayToBitArray(bytes: Array[Byte]): util.BitSet = {
    var bits:util.BitSet = new util.BitSet()
    bits = util.BitSet.valueOf(bytes)
    bits
  }

  def fromByteArray(bytes:Array[Byte]) : Long ={
    ByteBuffer.wrap(bytes).getLong()
  }
  def toByteArray(value:Long) : Array[Byte] ={
    ByteBuffer.allocate(4).putLong(value).array()
  }

  def leftS (R:util.BitSet, x: Int): util.BitSet ={
    val ROut:util.BitSet = new util.BitSet()

    for(i <- 0 until s){
      if(R.get(i)){
        if(i - x < 0){
          val s = i + x
          ROut.set(s)
        }
        else
          ROut.set(i - x)
      }
    }
    ROut
  }

  def f (R: util.BitSet, key: util.BitSet): util.BitSet ={
    var bitsOut:util.BitSet = leftS(R, 2)
    //print ln()
    //print ln("сдвиг 2:")
    //print ff(bitsOut,
    bitsOut.xor(leftS(R, 4))
    //print ln()
    //print ln("сдвиг 4:")
    //print ff(R,
    //print ln()
    //print ln("хор LxL")
    //print ff(bitsOut,
    //print ln()
    //print ln("key")
    //print ff(key, )
    bitsOut.xor(key)
    bitsOut
    //print ln()
    //print ln("f")
    //print ff(bitsOut,
    bitsOut
  }
  def printff(b:util.BitSet, len:Int): Unit ={
    for (i <- 0 until  len ) {
      if (i % 8 == 0)
        print(" ")
      if (b.get(i))
        print(1)
      else
        print(0)
    }

  }

  def g(b:util.BitSet, i:Int): util.BitSet={
    var ii = i*s
    var o:util.BitSet = new util.BitSet
    for(j <- ii until ii+s){
      if(b.get(j)){
        o.set(j - ii)
      }
    }
    o
  }
  def Haf(bitsInp:util.BitSet, bitsKey:util.BitSet, len:Int): util.BitSet ={

    val masL: Array[util.BitSet] = new Array[util.BitSet](len)
    val masR: Array[util.BitSet] = new Array[util.BitSet](len)
    val masLOut: Array[util.BitSet] = new Array[util.BitSet](len)
    val masROut: Array[util.BitSet] = new Array[util.BitSet](len)
    var jj = 1
    var ii = 0
    for(i <- 0 until len){
      masL(i) = g(bitsInp, ii)
      masR(i) = g(bitsInp, jj)
      jj+=2
      ii+=2
    }

    //print ln()
    /*for(i <- 0 until len){

      printff(masL(i),
      printff(masR(i),
    }
*/

    //1
    for(i <- 0 until len){
      masROut(i) = masL(i)
      //print ln()
      //print ln("R1")
      //print ff(masR(i),
      masR(i).xor(f(masL(i), bitsKey))
      //print ln()
      //print ln("L next")
      //print ff(masR(i),
      //print ln()
      //print ln("N:"+i)
      masLOut(i) =  masR(i)
      //print ff(masLOut(i),
      //print ff(masROut(i),
      //print ln()

      //print ln("========================")

    }
    var masOut:util.BitSet = new util.BitSet()
    ii = 0
    for(i <- 0 until len){
      for(j <- 0 until  s){
        if(masLOut(i).get(j)) masOut.set(ii)
        ii+=1
      }
      for(j <- 0 until s){
        if(masROut(i).get(j)) masOut.set(ii)
        ii+=1
      }
    }
    if(f){
      f = false; a = masOut.toByteArray}
    //print ln()
    //print ff(masOut, masOut.length())
    masOut
  }

  def Haf2(bitsInp:util.BitSet, bitsKey:util.BitSet, len:Int): util.BitSet ={

    var masL: Array[util.BitSet] = new  Array[util.BitSet](len)
    var masR: Array[util.BitSet] = new  Array[util.BitSet](len)
    var masLOut: Array[util.BitSet] = new  Array[util.BitSet](len)
    var masROut: Array[util.BitSet] = new  Array[util.BitSet](len)
    var jj = 1
    var ii = 0
    for(i <- 0 until len){
      masL(i) = g(bitsInp, ii)
      masR(i) = g(bitsInp, jj)
      jj+=2
      ii+=2
    }

    //print ln()
    /*for(i <- 0 until len){

      //print ff(masL(i),
      printff(masR(i),
    }
*/

    //1
    for(i <- 0 until len){
      masLOut(i) = masR(i)
      //print ln()
      //print ln("L1")
      //print ff(masR(i),
      masL(i).xor(f(masR(i), bitsKey))
      //print ln()
      //print ln("R prev")
      //print ff(masL(i),
      //print ln()
      //print ln("N:"+i)
      masROut(i) =  masL(i)
      //print ff(masLOut(i),
      //print ff(masROut(i),
      //print ln()

      //print ln("========================")

    }
    var masOut:util.BitSet = new util.BitSet()
    ii = 0
    for(i <- 0 until len){
      for(j <- 0 until  s){
        if(masLOut(i).get(j)) masOut.set(ii)
        ii+=1
      }
      for(j <- 0 until s){
        if(masROut(i).get(j)) masOut.set(ii)
        ii+=1
      }
    }
    if(f2){
      f2 = false; a = null; a = masOut.toByteArray}
    //print ln()
    //print ff(masOut, masOut.length())
    masOut
  }
  // f(x,k)= (x<<2)(+)(x<<4)(+)k
  def encryption(fileNameE:String):Unit = {

    val bytesInp = readBytes(new File(fileNameE))
    var bitsInp: util.BitSet = new util.BitSet()
    bitsInp = byteArrayToBitArray(bytesInp)

    //print ln()
    //print ln("bitsInp:")
    //print ff(bitsInp, lengthFileBits)

    var bitsKey = new util.BitSet()
    bitsKey = generateKey()
    //print ln()
    //print ln("key:")
    //print ff(bitsKey,

    //print ln()
    //print ln("--------11111111111111111111111111111---------------")

    var bitsOut1 = Haf(bitsInp, bitsKey, bytesInp.length)
    var bitsOut2 = Haf(bitsInp, bitsKey, bytesInp.length)
    val bitsOut3 = Haf(bitsInp, bitsKey, bytesInp.length)
    printStr(fileNameE,bitsOut3.toByteArray)


  }
  def decryption(filename:String): Unit ={
    var bitsKey = new util.BitSet()
    bitsKey = generateKey()
    val bytesInp2 = readBytes(new File(filename))
    var bitsInp2: util.BitSet = new util.BitSet()
    bitsInp2 = byteArrayToBitArray(bytesInp2)
    //print ln()
    //print ln("bitsInp2:")
    //print ff(bitsInp2, bitsInp2.length())
    val bitsOut21 = Haf2(bitsInp2, bitsKey, bytesInp2.length)
    val bitsOut22 = Haf2(bitsOut21, bitsKey, bytesInp2.length)
    val bitsOut23 = Haf2(bitsOut22, bitsKey, bytesInp2.length)
    printStr(filename, bitsOut23.toByteArray)

  }
  def main(args:Array[String]) :Unit ={

    val filenameE =  readLine("Input: ")// "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/lab6(sbt, scala)/src/main/resources/f.txt"

   // encryption(filenameE)//readLine("Input: ")
   decryption(filenameE)
  }































































}









































